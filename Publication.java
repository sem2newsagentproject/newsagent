import java.util.ArrayList;

public class Publication 
{
	public int pub_id;
	public double price;
	public String title;
	public String format;

	NewsAgentDAO s = new NewsAgentDAO();
	
	public Publication()
	{
	    pub_id = 0;
		price = 0;
		title = null;
		format = null;
	}
	
	public Publication(int pub_id,String title,String format, double price)
	{
		this.pub_id = pub_id;
		this.title = title;
		this.format = format;
		this.price = price;
		
	}
	
	@Override
	public String toString()
	{
		return "publication Id: "+pub_id + " " +"\nPrice: " + price +" " +"\nTitle: " +title  + "\nFormat: " +format+"\n\n";
	}
	
	

	
	public int getPubId() 
	{
		return pub_id;
	}
	public void setPubId(int pub_id) throws Exception
	{
		
		this.pub_id = pub_id;
		
	}
	
	public double getPrice() 
	{
		
		return price;
		
	}
	public void setPrice(double price) throws Exception
	{
		
		this.price = price;
		
	}
	
	public String getTitle() 
	{
		
		return title;
	}
	public void setTitle(String title) throws Exception
	{
		this.title = title;
	}
	
	public String getFormat() 
	{
		return format;
	}
	public void setFormat(String format) throws Exception
	{
		this.format = format;
	}
	


	public static void main(String[] args) 
	{
		
	}

}
