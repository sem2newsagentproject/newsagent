import junit.framework.TestCase;

public class InvoiceControllerTest extends TestCase {

	private NewsAgentDAO dao;
	
	InvoiceController i = new InvoiceController (dao);
	
	public  void testsendInvoice01()
	{
		//test InvId:1
		//test objective : valid
		//Inputs()  = 01
		//Expected outputs: valid

		
		Invoice test = new Invoice(01,00,"");
		
		try{

			assertEquals("01",i.sendInvoice(test));
			
		}
		catch(Exception s)
		{
			fail("Should not get here");
			
		}
	}
	
	public  void testsendInvoice02()
	{
		//test PubID:2
		//test objective : invalid
		//Inputs()  = -01
		//Expected outputs: invalid

		
		Invoice test = new Invoice(-01,00,"");
		
		try{

			assertEquals("-01",i.sendInvoice(test));
			
		}
		catch(Exception s)
		{
			fail("Invalid PubID");
			
		}
	}
	public  void testcreateInvoice01()
	{
		//test invId:1
		//test objective : valid
		//Inputs()  = 1
		//Expected outputs: valid

		
		Invoice test = new Invoice(01,00,"");
		
		try{

			assertEquals("1",i.createInvoice(test));
			
		}
		catch(Exception s)
		{
			fail("Invalid PubID");
			
		}
	}
	public  void testcreateInvoice02()
	{
		//test PubID:2
		//test objective : invalid
		//Inputs()  = -01
		//Expected outputs: invalid

		
		Invoice test = new Invoice(-01,00,"");
		
		try{

			assertEquals("-01",i.createInvoice(test));
			
		}
		catch(Exception s)
		{
			fail("Invalid PubID");
			
		}
	}
}
