import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class NewsAgentDAO
{
	public static Connection con;
	public static java.sql.Statement stat;
	//private static CustomerController c = new CustomerController();
	//private static EmployeeController e = new EmployeeController();
	//private static publicationController p = new publicationController();
	static Statement stmt = null;
	static ResultSet rs = null;
	static Scanner in = new Scanner(System.in);
	private static String phoneNumber;
	
	Statement getStatement() {
		return stmt;
	}
	
	public NewsAgentDAO()
	{
		try{
			
			init_db();
		}
		catch(Exception e)
		{
			System.out.println("Error connecting to database..");
			e.printStackTrace();
		}
	}

	public static List<Customer> getCustomers()
	{
		List<Customer> cusList = new ArrayList<Customer>();
		try
		{
			ResultSet rs = stmt.executeQuery("SELECT * FROM customer");
			
			while (rs.next())
			{
				int cusId = rs.getInt(1);
				String firstName = rs.getString(2);
				String lastName = rs.getString(3);
				String address1 = rs.getString(4);
				String address2 = rs.getString(5);
				String phoneNumber = rs.getString(6);
				String postcode = rs.getString(7);
				Address add = new Address(address1, address2);
				Customer cus = new Customer(cusId, firstName, lastName, add, phoneNumber, postcode);
				cusList.add(cus);
			}
		}
		catch(Exception e)
		{
			System.out.println("Error Getting");
			e.printStackTrace();
		}
		return cusList;
	}
	
	
	public static List<Employee> getEmployees()
	{
		List<Employee> empList = new ArrayList<Employee>();
		try
		{
			ResultSet rs = stmt.executeQuery("SELECT * FROM employee");
			
			while (rs.next())
			{
				int EmpID = rs.getInt(1);
				String firstname = rs.getString(2);
				String lastname = rs.getString(3);
				String address1 = rs.getString(4);
				String address2 = rs.getString(5);
				String phonenum = rs.getString(6);
				Address add = new Address(address1, address2);
				Employee emp = new Employee(EmpID ,firstname, lastname, add, phonenum);
				empList.add(emp);
			}
		}
		catch(Exception e)
		{
			System.out.println("Error Getting");
			e.printStackTrace();
		}
		return empList;
	}
	
	public static List<Publication> getPublication()
	{
		List<Publication> pubList = new ArrayList<Publication>();
				try
				{
					ResultSet rs = stmt.executeQuery("SELECT * FROM publication");
					
					while(rs.next())
					{
						int pubId = rs.getInt(1);
						double price = rs.getDouble(3);
						String title = rs.getString(2);
						String format = rs.getString(4);						
						Publication pub = new Publication(pubId, title, format, price);
						pubList.add(pub);
					}
				}
				catch(Exception e)
				{
					System.out.println("Error Getting");
					e.printStackTrace();
				}
				return pubList;
	}
	
	public static List<Subscription> getSubscription()
	{
		List<Subscription> subList = new ArrayList<Subscription>();
				try
				{
					ResultSet rs = stmt.executeQuery("SELECT * FROM subscription");
					
					while(rs.next())
					{
						int subId = rs.getInt(1);
						int cus_id = rs.getInt(2);
						int pub_id = rs.getInt(3);
						String freq = rs.getString(4);
						
						Subscription sub = new Subscription(subId, cus_id, pub_id, freq);
						subList.add(sub);
					}
				}
				catch(Exception e)
				{
					System.out.println("Error Getting");
					e.printStackTrace();
				}
				return subList;
	}
	
	
	public static void addNewCustomer(Customer cust)
	{
		
		try {
		//c.addCustomer(cust);
		//int i  =  c.getSize() - 1;
		//c.getCustomer(i).getCusId();
		
		//int cusId = c.getCustomer(0).getCusId();
		String firstName = cust.getFirstName();
		String lastName = cust.getLastName();
		Address address = cust.getAddress();
		//String address2 = c.getCustomer(i).getAddress2();
		String phoneNumber = cust.getPhoneNumber();
		String postCode = cust.getPostcode();
		
        String str="INSERT INTO customer VALUES ('"+0+"', '"+firstName+"', '"+lastName+"', '"+address.getAddress1()+"', '"+address.getAddress2()+"', '"+phoneNumber+"', '"+postCode+"');";
        
			stmt.executeUpdate(str);
		} catch (SQLException e) {
			System.out.println("Error adding customer");
			e.printStackTrace();
		}
	}
	
	
	public void addNewEmployee(Employee emp)
	{
		Statement localstmt = getStatement();
		
		try {
		/*e.addEmployee(emp);
		int i  =  e.getSize() - 1;
		e.viewEmployee(i).getEmpID();*/
		
		//int cusId = c.getCustomer(0).getCusId();
		String firstname = emp.getFirstName();
		String lastname = emp.getLastName();
		Address address = emp.getAddress();
		//String address2 = c.getCustomer(i).getAddress2();
		String phonenum = emp.getPhoneNumber();
		
        String str="INSERT INTO employee VALUES ('"+0+"', '"+firstname+"', '"+lastname+"', '"+address.getAddress1()+"', '"+address.getAddress2()+"', '"+phonenum+"');";
        
			localstmt.executeUpdate(str);
			return;
		} catch (SQLException e) {
			System.out.println("Error adding employee");
			e.printStackTrace();
			return;
		}
	}
	
	public void addPublication(Publication pub) 
	{
		 try {
		//p.addPublication(pub);
		//int i  =  p.getSize() - 1;
		//p.getPublication(i).getPubId();
		
		//int pubId = p.getPublication(0).getPubId();
		double price = pub.getPrice();
		String title = pub.getTitle();
		String format = pub.getFormat();
		
		String str = "INSERT INTO publication VALUES ('"+0+"', '"+title+"',  '"+price+"','"+format+"');";
       
			stmt.executeUpdate(str);
		} catch (SQLException e) {
			System.out.println("Error adding publication");
			e.printStackTrace();
		}
	}
	
	public void addSubscription(Subscription sub) 
	{
	   try {
		int cus_id = sub.getCus_id();
		int pub_id = sub.getPubId();
		String freq = sub.getfreq();
		/*
		int cusId=-1;
		int pubId=-1;
		
		try
		{
			ResultSet rs = stmt.executeQuery("SELECT * FROM subscription where sub_id="+sub_id+";");
			
			while(rs.next())
			{
				
				cusId = rs.getInt(2);
				pubId = rs.getInt(3);
			}
		}
		catch(Exception e)
		{
			System.out.println("Error Getting");
			e.printStackTrace();
		}
		
		if(cus_id!=cusId)
		{
			System.out.println("Cus_id doesnt exist");
		}
		else if(pub_id!=pubId)
		{
			System.out.println("Pub_id doesnt exist");
			
		}
		else{*/
		
		
		String str = "INSERT INTO subscription VALUES ('"+0+"', '"+cus_id+"',  '"+pub_id+"','"+freq+"');";
       
			stmt.executeUpdate(str);
		} 
	   catch (SQLException e) {
			System.out.println("Error adding subscription");
			e.printStackTrace();
		}
	}
	
	public static void removeCustomer(int i) throws SQLException
	{
		int toRemove = i;
		String str1 = "DELETE FROM subscription WHERE cus_id = "+toRemove+";";
		String str = "DELETE FROM customer WHERE cus_id = "+toRemove+";";
		
		stmt.executeUpdate(str1);
		stmt.executeUpdate(str);
	}
	
	public static void removeEmployee(int i) throws SQLException
	{
		int toRemove = i;
		String str = "DELETE FROM employee WHERE emp_id = "+toRemove+";";
		
		stmt.executeUpdate(str);
	}
	
	public static void removePublication(int i) throws SQLException
	{
		int toRemove = i;
		String str = "DELETE FROM publication WHERE publication_id = "+toRemove+";";
		
		stmt.executeUpdate(str);
	}
	
	public static void removeSubscription(int i) throws SQLException
	{
		int toRemove = i;
		String str = "DELETE FROM subscription WHERE sub_id = "+toRemove+";";
		
		stmt.executeUpdate(str);
	}
	
	public void editCustomer(Customer cus)
	{
		Statement localstmt = getStatement();
		
		try {
			
			int cus_id = cus.getCusId();
			String firstname = cus.getFirstName();
			String lastname = cus.getLastName();
			Address address = cus.getAddress();
			String phonenum = cus.getPhoneNumber();
			String postCode = cus.getPostcode();
			
			String str1 = "Update customer SET firstName='"+firstname+"', lastName='"+lastname+"', address1='"+address.getAddress1()+"', address2='"+address.getAddress2()+"', phone_No='"+phonenum+"', postcode='"+postCode+"' WHERE cus_id = "+cus_id+";";
			localstmt.executeUpdate(str1);
			
				return;
		} 
		catch (SQLException c) {
			System.out.println("Error adding customer");
			c.printStackTrace();
			return;}
	}
	
	public void editEmployee(Employee emp)
	{
		Statement localstmt = getStatement();
		
		try {
			
			int emp_id = emp.getEmpID();
			String firstname = emp.getFirstName();
			String lastname = emp.getLastName();
			Address address = emp.getAddress();
			String phonenum = emp.getPhoneNumber();
			
			String str1 = "Update employee SET firstName='"+firstname+"', lastName='"+lastname+"', address1='"+address.getAddress1()+"', address2='"+address.getAddress2()+"', phone_No='"+phonenum+"' WHERE emp_id = "+emp_id+";";
			localstmt.executeUpdate(str1);
			
			//str2 to set to null first
			//String str2 ="";
			
	       //String str3="INSERT INTO customer VALUES ('"+cus_id+"', '"+firstname+"', '"+lastname+"', '"+address.getAddress1()+"', '"+address.getAddress2()+"', '"+phonenum+"', '"+postCode+"');";
	        
				//localstmt.executeUpdate(str3);
				return;
		} 
		catch (SQLException c) {
			System.out.println("Error adding employee");
			c.printStackTrace();
			return;}
	}
	
	
	public void editPublication(Publication pub)
	{
		Statement localstmt = getStatement();
		
		try {
			
			int pub_id = pub.getPubId();
			double price = pub.getPrice();
			String title = pub.getTitle();
			String format = pub.getFormat();
			
			String str1 = "Update publication SET title='"+title+"', price='"+price+"', format='"+format+"' WHERE publication_id = "+pub_id+";";
			localstmt.executeUpdate(str1);
			
			//str2 to set to null first
			//String str2 ="";
			
	       //String str3="INSERT INTO customer VALUES ('"+cus_id+"', '"+firstname+"', '"+lastname+"', '"+address.getAddress1()+"', '"+address.getAddress2()+"', '"+phonenum+"', '"+postCode+"');";
	        
				//localstmt.executeUpdate(str3);
				return;
		} 
		catch (SQLException c) {
			System.out.println("Error adding publication");
			c.printStackTrace();
			return;}
	}
	
	public void editSubscription(Subscription sub)
	{
		Statement localstmt = getStatement();
		
		try {
			int sub_id = sub.getSubId();
			int cus_id = sub.getCus_id();
			int pub_id = sub.getPubId();
			String freq = sub.getfreq();
			
			int cusId=-1;
			int pubId=-1;
			
			
			try
			{
				ResultSet rs = stmt.executeQuery("SELECT * FROM subscription where sub_id="+sub_id+";");
				
				while(rs.next())
				{
					
					cusId = rs.getInt(2);
					pubId = rs.getInt(3);
				}
			}
			catch(Exception e)
			{
				System.out.println("Error Getting");
				e.printStackTrace();
			}
			
			if(cus_id!=cusId)
			{
				System.out.println("Cus_id doesnt exist");
			}
			else if(pub_id!=pubId)
			{
				System.out.println("Pub_id doesnt exist");
				
			}
			else{
				String str1 = "Update subscription SET cus_id='"+cus_id+"', publication_id='"+pub_id+"', frequency='"+freq+"' WHERE sub_id = "+sub_id+";";
				localstmt.executeUpdate(str1);
			}
			
				return;
		} 
		catch (SQLException c) {
			System.out.println("Error adding publication");
			c.printStackTrace();
			return;}
	}
	
	public static void main(String[] args) throws SQLException 
	{
		
		  
	}
	public static void init_db()
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			String url="jdbc:mysql://localhost:3307/newsagents";
			con = DriverManager.getConnection(url, "root", "admin");
			stmt = con.createStatement();
		}
		catch(Exception e)
		{
			System.out.println("Error: Failed to connect to database\n"+e.getMessage());
		}
	}
}