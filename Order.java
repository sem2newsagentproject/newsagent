public class Order 
{
	public int cusId;
	public int pubId;
	public int frequency;

	public Order(int cusId, int pubId, int frequency)
	{
		this.cusId = cusId;
		this.pubId = pubId;
		this.frequency = frequency;
	}
	
	public int getCusId() 
	{
		return cusId;
	}
	public void setCusId(int cusId) 
	{
		this.cusId = cusId;
	}
	
	public int getPubId() 
	{
		return pubId;
	}
	public void setPubId(int pubId) 
	{
		this.pubId = pubId;
	}
	
	public int getFrequency() 
	{
		return frequency;
	}
	public void setFrequency(int frequency) 
	{
		this.frequency = frequency;
	}
	
	public static void main(String[] args) 
	{

	}

}
