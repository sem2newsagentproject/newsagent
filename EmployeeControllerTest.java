import junit.framework.TestCase;


public class EmployeeControllerTest extends TestCase {
	
	private NewsAgentDAO dao;
		
		
		public  void testAddEmployee01()
		{
			//test firstName:1
			//test objective : valid
			//Inputs()  = brendan
			//Expected outputs: valid

			NewsAgentDAO dao = new NewsAgentDAO();
			EmployeeController testObject = new EmployeeController(dao);
			
			
			
			try
				{
				
					Employee test = new Employee(0, "Brendan", "keane",new Address("Willow24" , "House"), "087655420");
					assertEquals(true,testObject.addEmployee(test));
				}
			catch(EmployeeExceptionHandler e)
				{
					fail("Should not get here");
				}
		}
		
		public  void testAddEmployee02()
		{
			//test firstName:2
			//test objective : has incorrect characters
			//Inputs()  = brendan12
			//Expected outputs: invalid

EmployeeController testObject = new EmployeeController(dao);
			
			try {
				Employee test = new Employee(0, "brendan12", "keane",new Address("Willow24" , "House"), "087655420");

				  testObject.addEmployee(test);
				fail("Should not get here ... Exception is expected");
				
			}
			catch (EmployeeExceptionHandler e) {
				
				assertEquals("Invalid First Name : Wrong Characters", e.getMessage());
				
			}
		}
		
		public  void testAddEmployee03()
		{
			//test firstName:3
			//test objective : string too long
			//Inputs()  = toolongofatestname
			//Expected outputs: invalid

			EmployeeController testObject = new EmployeeController(dao);
			
			try {
				Employee test = new Employee(0, "toolongofatestname", "keane",new Address("Willow24" , "House"), "087655420");

				  testObject.addEmployee(test);
				fail("Should not get here ... Exception is expected");
				
			}
			catch (EmployeeExceptionHandler e) {
				
				assertEquals("First Name : Too many characters", e.getMessage());
				
			}
		}

		
		public  void testAddEmployee04()
		{
			//test lastName:1
			//test objective : correct
			//Inputs()  = keane
			//Expected outputs: valid

			NewsAgentDAO dao = new NewsAgentDAO();
			EmployeeController testObject = new EmployeeController(dao);
			
			try {
				Employee test = new Employee(0, "Brendan", "keane",new Address("Willow24" , "House"), "087655420");

				  testObject.addEmployee(test);
				
				
			}
			catch (EmployeeExceptionHandler e) {
				
				fail("Should not get here ... Exception is expected");
				
			}
		}

		public  void testAddEmployee05()
		{
			//test lastName:2
			//test objective : incorrect characters
			//Inputs()  = keane37
			//Expected outputs: invalid

			EmployeeController testObject = new EmployeeController(dao);
			
			try {
				Employee test = new Employee(0, "Brendan", "keane37",new Address("Willow24" , "House"), "087655420");

				  testObject.addEmployee(test);
				fail("Should not get here ... Exception is expected");
				
			}
			catch (EmployeeExceptionHandler e) {
				
				assertEquals("Invalid Last Name : Wrong Characters", e.getMessage());
				
			}
		}
		public  void testAddEmployee06()
		{
			//test lastName:3
			//test objective : too long last name
			//Inputs()  = toolongofalastname
			//Expected outputs: invalid

			EmployeeController testObject = new EmployeeController(dao); 
			
			try {
				Employee test = new Employee(0, "Brendan", "toolongofalastname",new Address("Willow24" , "House"), "087655420");

				  testObject.addEmployee(test);
				fail("Should not get here ... Exception is expected");
				
			}
			catch (EmployeeExceptionHandler e) {
				
				assertEquals("Last Name : Too many Characters", e.getMessage());
				
			}
		}

		public  void testAddEmployee07()
		{
			//test address:1
			//test objective : valid
			//Inputs()  = willow
			//Expected outputs: valid

			NewsAgentDAO dao = new NewsAgentDAO();
			EmployeeController testObject = new EmployeeController(dao);
			
			try {
				Employee test = new Employee(0, "Brendan", "keane",new Address("Willow" , "House24"), "087655420");

				assertEquals(true,testObject.addEmployee(test));
				
			}
			catch (EmployeeExceptionHandler e) {
				fail("Should not get here ... Exception is expected");
				
			}
		}

		public  void testAddEmployee08()
		{
			//test address1:2
			//test objective : invalid
			//Inputs()  = willow24istoolongofanaddress
			//Expected outputs: too many characters

			EmployeeController testObject = new EmployeeController(dao);
			
			try {
				Employee test = new Employee(0, "Brendan", "keane",new Address(" willow24istoolongofanaddress" , "House"), "087655420");
				  testObject.addEmployee(test);
				fail("Should not get here ... Exception is expected");
				
			}
			catch (EmployeeExceptionHandler e) {
				
				assertEquals("Address : To many characters", e.getMessage());
				
			}
		}

		

		public  void testAddEmployee9()
		{
			//test address2:1
			//test objective : valid
			//Inputs()  = House
			//Expected outputs: valid

			NewsAgentDAO dao = new NewsAgentDAO();
			EmployeeController testObject = new EmployeeController(dao);
			
			try {
				Employee test = new Employee(0, "Brendan", "keane",new Address("Willow24" , "House"), "087655420");
				
				boolean isAdded = false; 		
			testObject.addEmployee(test);
				
			assertEquals(true,testObject.addEmployee(test));
				
			}
			catch (EmployeeExceptionHandler e) {
				
				fail("Should not get here ... Exception is expected");
				
			}
		}

		public  void testAddEmployee10()
		{
			//test address2:2
			//test objective : too many characters
			//Inputs()  = toolongofasecondaddress
			//Expected outputs: valid

			EmployeeController testObject = new EmployeeController(dao);
			
			try {
				Employee test = new Employee(0, "Brendan", "keane",new Address("Willow24" , "toolongofasecondaddress"), "087655420");

				  testObject.addEmployee(test);
				fail("Should not get here ... Exception is expected");
				
			}
			catch (EmployeeExceptionHandler e) {
				
				assertEquals("Address : To many characters", e.getMessage());
				
			}
		}

		

		public  void testAddEmployee11()
		{
			//test phonenum:1
			//test objective : valid
			//Inputs()  = 0876551620
			//Expected outputs: valid

			NewsAgentDAO dao = new NewsAgentDAO();
			EmployeeController testObject = new EmployeeController(dao);
			
			try {
				Employee test = new Employee(0, "Mike", "Russell",new Address("Willow24" , "House"), "0876551620");

				assertEquals(true,testObject.addEmployee(test));
				
			}
			catch (EmployeeExceptionHandler e) {
				
				fail("Should not get here ... Exception is expected");
			}
		}

		public  void testAddEmployee12()
		{
			//test phonenum:2
			//test objective : has a letter
			//Inputs()  = jerry
			//Expected outputs: valid

			EmployeeController testObject = new EmployeeController(dao);
			
			try {
				Employee test = new Employee(0, "Brendan", "keane",new Address("Willow24" , "House"), "0876554bk");

				  testObject.addEmployee(test);
				fail("Should not get here ... Exception is expected");
				
			}
			catch (EmployeeExceptionHandler e) {
				
				assertEquals("Invalid Phonenumber : Must only contain numbers", e.getMessage());
				
			}
		}

		public  void testAddEmployee13()
		{
			//test phonenum:3
			//test objective : too long
			//Inputs()  = 087655420420
			//Expected outputs: invalid

			EmployeeController testObject = new EmployeeController(dao);
			
			try {
				Employee test = new Employee(0, "Brendan", "keane",new Address("Willow24" , "House"), "087655420420");

				  testObject.addEmployee(test);
				fail("Should not get here ... Exception is expected");
				
			}
			catch (EmployeeExceptionHandler e) {
				
				assertEquals("Too many digits", e.getMessage());
				
			}
		}


}