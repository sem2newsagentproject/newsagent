import junit.framework.TestCase;


public class DeliveryContentControllerTest extends TestCase 
{
	private NewsAgentDAO dao;
	
	DeliveryContentController d = new DeliveryContentController(dao);
	
	//d.addDeliveryContent(new DeliveryContent(12,15);
	
	
	public  void testEditDeliveryContent01()
	{
		//test PubID:1
		//test objective : valid
		//Inputs()  = 01
		//Expected outputs: valid

		
		DeliveryContent test = new DeliveryContent(01,00);
		
		try{

			assertEquals("01",d.editDeliveryContent(test));
			
		}
		catch(Exception d)
		{
			fail("Should not get here");
			
		}
	}
	
	public  void testEditDeliveryContent02()
	{
		//test PubID:2
		//test objective : invalid
		//Inputs()  = -01
		//Expected outputs: invalid

		
		DeliveryContent test = new DeliveryContent(-01,00);
		
		try{

			assertEquals("-01",d.editDeliveryContent(test));
			
		}
		catch(Exception d)
		{
			fail("Invalid PubID");
			
		}
	}
	
	public  void testEditDeliveryContent03()
	{
		//test DelID:1
		//test objective : valid
		//Inputs()  = 01
		//Expected outputs: valid

		
		DeliveryContent test = new DeliveryContent(00,01);
		
		try{

			assertEquals("01",d.editDeliveryContent(test));
			
		}
		catch(Exception d)
		{
			fail("Should not get here");
			
		}
	}
	
	public  void testEditDeliveryContent04()
	{
		//test DelID:2
		//test objective : invalid
		//Inputs()  = -01
		//Expected outputs: invalid

		
		DeliveryContent test = new DeliveryContent(00,-01);
		
		try{

			assertEquals("-01",d.editDeliveryContent(test));
			
		}
		catch(Exception d)
		{
			fail("Invalid DelID");
			
		}
	}
}
