public class Customer 
{
	public int cusId;
	public String firstName;
	public String lastName;
	public Address address;
	public String phoneNumber;
	public String postcode;
	
	//NewsAgentDAO s=new NewsAgentDAO();
	
	public Customer()
	{
		cusId = 0;
		firstName = null;
		lastName = null;
		address = null;
		phoneNumber = null;
		postcode = null;
	}
	public Customer(int cusId, String firstName, String lastName, Address address, String phoneNumber, String postcode)
	{
		this.cusId = cusId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.postcode = postcode;	
	}
	
	
	
	@Override
	public String toString() {
		return "Customer [cusId=" + cusId + "\n, firstName=" + firstName
				+ "\n, lastName=" + lastName + "\n, address=" + address
				+ ", phoneNumber=" + phoneNumber
				+ "\n, postcode=" + postcode + "]\n\n";
	}
	
	/*public void Customer(int cusId, String firstName, String lastName, String address1, String address2, String phoneNumber, String postcode)
	{
			this.cusId = cusId;
			this.firstName = firstName;
			this.lastName = lastName;
			this.address1 = address1;
			this.address2 = address2;
			this.phoneNumber = phoneNumber;
			this.postcode = postcode;	
			
	}
	
	public void addCustomer(int cusId, String firstName, String lastName, String address1, String address2, String phoneNumber, String postcode)
	{
		 Customer a= new Customer();
			this.cusId = cusId;
			this.firstName = firstName;
			this.lastName = lastName;
			this.address1 = address1;
			this.address2 = address2;
			this.phoneNumber = phoneNumber;
			this.postcode = postcode;	
			
			s.AddCustomer(a);
	}
	 */
	public int getCusId() 
	{
		return cusId;
	}
	public void setCusId(int cusId) 
	{
		this.cusId = cusId;
	}

	public String getFirstName() 
	{
		return firstName;
	}
	public void setFirstName(String firstName)  
	{
		this.firstName = firstName;
	}

	public String getLastName() 
	{
		return lastName;
	}
	public void setLastName(String lastName) 
	{
		this.lastName = lastName;
	}

	

	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public String getPhoneNumber() 
	{
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) 
	{
		this.phoneNumber = phoneNumber;
	}

	public String getPostcode() 
	{
		return postcode;
	}
	public void setPostcode(String postcode) 
	{
		this.postcode = postcode;
	}

	public static void main(String[] args) 
	{

	}

}
