public class Subscription
{
	public int publication_id;
	public int customer_id;
	public String frequency;
	public int subscription_id;

	NewsAgentDAO s = new NewsAgentDAO();
	
	public Subscription()
	{
		subscription_id = 0;
		publication_id = 0;
		customer_id = 0;
		frequency = null;
	}
	
	public Subscription(int sub_id, int cus_id, int pub_id, String freq)
	{
		this.subscription_id = sub_id;
		this.publication_id = pub_id;
		this.customer_id = cus_id;
		this.frequency = freq;
	}
	
	@Override
	public String toString()
	{
		return "Subscription contains: Subscription id : "+subscription_id+"\n Publication "+publication_id + ",\n  Customer: " + customer_id +",\n  Frequency: " +frequency +"\n\n";
	}
	
	Customer cus = new Customer();
	
	public int getSubId() 
	{
		return subscription_id;
	}
	public void setSubId(int sub_id) 
	{
		this.subscription_id = sub_id;	
	}
	
	public int getPubId() 
	{
		return publication_id;
	}
	public void setPubId(int pub_id) 
	{
		this.publication_id = pub_id;	
	}
	
	public int getCus_id() 
	{
		return customer_id;	
	}
	
	public void setCus_id(int cusid)
	{
		this.customer_id = cusid;
	}
	
	public String getfreq() 
	{
		return frequency;
		
	}
	
	public void setfreq(String freq)
	{
		
		frequency=freq;
	}
	public void getAddress(int sub_id)
	{
		
		//cus.getCusId().getAddress();
	}


	public static void main(String[] args) 
	{
		
	}

}