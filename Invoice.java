public class Invoice 
{
	public int invId;
	public double total;
	public String date;

	public Invoice()
	{
		invId = 0;
		total = 0;
		date = null;
	}
	public Invoice(int invId, double total, String date)
	{
		this.invId = invId;
		this.total = total;
		this.date = date;
	}
	
	public String toString1() {
		return "Invoice [invId=" + invId + ", total=" + total
				+ ", date=" + date + "]";
	}
	public int getInvId() 
	{
		return invId;
	}
	public void setInvId(int invId) 
	{
		this.invId = invId;
	}
	
	public double getTotal() 
	{
		return total;
	}
	public void setTotal(double total) 
	{
		this.total = total;
	}
	
	public String getDate() 
	{
		return date;
	}
	public void setDate(String date) 
	{
		this.date = date;
	}
	public String toString()
	{
		return "invId:"+invId+ ""+total+ ""+date;
	}
	public static void main(String[] args) 
	{
	
	}

}
