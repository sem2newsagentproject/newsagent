
public class InvoiceExceptionHandler extends Exception {
	String message;
	
	public InvoiceExceptionHandler(String invoiceMessage){
		message = invoiceMessage;
	}
	
	public String getMessage() {
		return message;
	}
}
