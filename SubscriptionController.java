
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.regex.Pattern;


public class SubscriptionController {
	
	public SubscriptionController(NewsAgentDAO dao)
	{
		this.dao = dao;
	}
	
	
	private NewsAgentDAO dao;
	
	//int cusId, String firstName, String lastName, Address address, String phoneNumber, String postcode
	
	public boolean addSubscription(Subscription s) throws SubscriptionExceptionHandler
	{
		 boolean isAdded = false;
		 String frequency = s.getfreq();
		 
			
		
		 if ( (frequency.equals("daily") ||frequency.equals("weekly") || frequency.equals("monthly"))){ 
			 	dao.addSubscription(s);
				isAdded = true;
		 	
			}
		
		 else {
			throw new SubscriptionExceptionHandler("Invalid frequency, Please enter('daily', 'weekly', or 'monthly')");
		 }
		 return isAdded;
	}
	
	public void removeSubscription(int i)
	{
		
		try {
			dao.removeSubscription(i);
		} catch (SQLException s) {
			
			s.printStackTrace();
		}
		
	}
	
	public boolean editSubscription(Subscription s) throws SubscriptionExceptionHandler
	{	
		 boolean canEdit = false;
		
		
		int cus_id = s.getCus_id();
		int pub_id = s.getPubId();
		String freq  =s.getfreq();
		
		 
		 
		if ( (cus_id<=0)){ 
				throw new SubscriptionExceptionHandler("Invalid cus_id");}
		 
		if ( (pub_id<=0)){ 
			throw new SubscriptionExceptionHandler("Invalid pub_id");}
		
		if ( (Pattern.matches("[a-zA-Z]+", s.getfreq()) != true)){ 
			throw new SubscriptionExceptionHandler("Invalid frequency");}
		
		else {
			dao.editSubscription(s);
			canEdit = true;
			
		}
		return canEdit;
	}
	

}
