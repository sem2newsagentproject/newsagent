import java.util.regex.Pattern;



public class Employee {

	private String firstname;
	private String lastname;
	private Address address;
	private String phonenum;
	private int EmpID;
	
	public Employee()
	{
		firstname = null;
		lastname = null;
		address = null;
		phonenum = null;
		EmpID = 0;
	}
	public Employee(int i, String fn, String ln, Address address, String pn) 
	{
		firstname = fn;	
		lastname = ln;
		this.address = address;
		phonenum = pn;
		EmpID = i;
	}
	
	public String toString()
	{
		return "First Name: " +firstname + "\n Last Name ; " +lastname +"\n Address: " +address+ "\n PhoneNum: " +phonenum+"\n EmpNo: " + EmpID+"\n\n";
	}
	
	//employee id
	public int getEmpID() 
	{
		return EmpID;
	}
	public void setEmpID(int EmpID) 
	{
		this.EmpID = EmpID;
	}

	//first name
	public  String getFirstName() 
	{
		return firstname;
	}
	public void setFirstName(String firstName) 
	{
		this.firstname = firstName;			
	}

	//last name
	public String getLastName() 
	{
		return lastname;
	}
	public void setLastName(String lastName)  
	{
		this.lastname = lastName;
	}

	//address
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	
	//phonenumber
	public String getPhoneNumber() 
	{
		return phonenum;
	}
	public void setPhoneNumber(String phoneNumber) 
	{
		this.phonenum = phoneNumber;
	}
	 
	/*public static void main (String [] args) throws EmployeeExceptionHandler
	{
		Employee a = new Employee();
		
		a.setFirstName("Jerry");
		a.setLastName("Keane");
		a.setAddress(new Address("Willow","House"));
		a.setPhoneNumber("4206969");
		
		int number = a.getEmpID();
		 int fnlength = a.getFirstName().length();
		 int lnlength = a.getLastName().length();
		 
		 int numlength =a.getPhoneNumber().length();
		 
		 int alength = a.toString().length();
		 
		 
		if( ( "" + number ).contains( "[a-zA-Z]" ) ) {
			throw new EmployeeExceptionHandler("Should only contain numbers");}
		
		else if(number < 0){
			throw new EmployeeExceptionHandler("Cannot be less than zero");}
			
		
		else if ( (Pattern.matches("[a-zA-Z]+", a.getFirstName()) != true)){ 
			throw new EmployeeExceptionHandler("Should only contain letters");}
			
		else if(fnlength > 15){
				throw new EmployeeExceptionHandler("To many characters");}
		
		else if ( (Pattern.matches("[a-zA-Z]+", a.getLastName()) != true)){ 
			throw new EmployeeExceptionHandler("Should only contain letters");}
			
		else if(lnlength > 15){
				throw new EmployeeExceptionHandler("To many characters");}
			
		/else if(alength > 50){
			throw new EmployeeExceptionHandler("To many characyers");}
		
		else if ( (Pattern.matches("[0-9]+", a.getPhoneNumber()) != true)){ 
			throw new EmployeeExceptionHandler("Invalid first name");}
			
		else if(numlength > 10){
				throw new EmployeeExceptionHandler("To many characyers");
				
				}
		
		
		else {
			System.out.print("Men");
		}
				}*/
}
