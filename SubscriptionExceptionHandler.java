
public class SubscriptionExceptionHandler extends Exception {

	String message;
	
	public SubscriptionExceptionHandler(String subscriptionMessage){
		message = subscriptionMessage;
	}
	
	public String getMessage() {
		return message;
	}


}
