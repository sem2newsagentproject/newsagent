import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Pattern;


public class EmployeeController {
	
private NewsAgentDAO dao;


	
	public EmployeeController(NewsAgentDAO dao)
	{
		this.dao = dao;
	}
	
	NewsAgentDAO getNewsAgentDAO() {
		return dao;
	}
	
	public boolean addEmployee(Employee e) throws EmployeeExceptionHandler
	{
		boolean isAdded = false;
		
		 int fnlength = e.getFirstName().length();
		 int lnlength = e.getLastName().length();
		 
		 int numlength =e.getPhoneNumber().length();
		 
		 int alength = e.getAddress().toString().length();
		 
		 
		
		if ( (Pattern.matches("[a-zA-Z]+", e.getFirstName()) != true)){ 
			throw new EmployeeExceptionHandler("Invalid First Name : Wrong Characters");}
			
		else if(fnlength > 15){
				throw new EmployeeExceptionHandler("First Name : Too many characters");}
		
		else if ( (Pattern.matches("[a-zA-Z]+", e.getLastName()) != true)){ 
			throw new EmployeeExceptionHandler("Invalid Last Name : Wrong Characters");}
			
		else  if(lnlength > 15){
				throw new EmployeeExceptionHandler("Last Name : Too many Characters");}
			
		else if(alength > 20){
			throw new EmployeeExceptionHandler("Address : To many characters");}
		
		else if ( (Pattern.matches("[0-9]+", e.getPhoneNumber()) != true)){ 
			throw new EmployeeExceptionHandler("Invalid Phonenumber : Must only contain numbers");}
			
		else if(numlength > 10){
				throw new EmployeeExceptionHandler("Too many digits");}
		
		
		else {
			dao.addNewEmployee(e);
			isAdded = true;
			
		}
		return isAdded;
	}
	
	public Employee viewEmployee(int i)
	{
		return null;
		
	}
	
	public void removeEmployee(int i)
	{
		//try{
		try {
			dao.removeEmployee(i);
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		//}
		//catch
		//{
		//	System.out.println("Error removing Employee");
		//}
	}
	
	public boolean editEmployee(Employee e) throws EmployeeExceptionHandler
	{	
		 boolean canEdit = false;
		
		
		 int fnlength = e.getFirstName().length();
		 int lnlength = e.getLastName().length();
		 int numlength =e.getPhoneNumber().length();
		 int alength = e.getAddress().toString().length();
		 
		 
		 //if ( (c.getCusId()).isDigit() != true)){ 
				//throw new CustomerExceptionHandler("Invalid First Name to change: Wrong Characters");}
		 
		if ( (Pattern.matches("[a-zA-Z]+", e.getFirstName()) != true)){ 
			throw new EmployeeExceptionHandler("Invalid First Name to change: Wrong Characters");}
			
		else if(fnlength > 15){
				throw new EmployeeExceptionHandler("First Name : Too many characters");}
		
		else if ( (Pattern.matches("[a-zA-Z]+", e.getLastName()) != true)){ 
			throw new EmployeeExceptionHandler("Bananaaaaaas");}//Invalid Last Name to change: Wrong Characters
			
		else  if(lnlength > 15){
				throw new EmployeeExceptionHandler("Last Name : Too many Characters");}
			
		else if(alength > 20){
			throw new EmployeeExceptionHandler("Invalid Address to change: To many characters");}
		
		else if ( (Pattern.matches("[0-9]+", e.getPhoneNumber()) != true)){ 
			throw new EmployeeExceptionHandler("Invalid Phonenumber to change: Must only contain numbers");}
			
		else if(numlength > 10){
				throw new EmployeeExceptionHandler("Too many digits");}
		
		
		else {
			dao.editEmployee(e);
			canEdit = true;}
			
	
		return canEdit;
	}

	
	
	
	//add/remove/edit/view/reset

	
}
