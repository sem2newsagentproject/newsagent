import junit.framework.TestCase;


public class CustomerControllerTest extends TestCase {
	
	private NewsAgentDAO dao;
	
		//EmployeeController e = new EmployeeController();
		
		//test add employee
		//e.addEmployee(new Employee("Brendan", "keane",new Address("Willow" , "House"), "087655420", 1576));
		
		//e.addEmployee(new Employee("Joe", "Noone",new Address("Longford" , "House"), "0876553793", 1577));
		
		
		
		public  void testAddCustomer01()
		{
			//test firstName:1
			//test objective : valid
			//Inputs()  = Kyle
			//Expected outputs: valid
				
			NewsAgentDAO dao = new NewsAgentDAO();
			CustomerController testObject = new CustomerController(dao);
			
			try {
				Customer test = new Customer(0, "Kyle", "Stewart",new Address("the moon" , "boiling rock"), "08367632","postcode16");

				assertEquals(true,testObject.addCustomer(test));
				
				
			}
			catch (CustomerExceptionHandler c) {
				
				fail("Should not get here ... Exception is expected");
				
				
			}
		}
		
		public  void testAddCustomer02()
		{
			//test firstName:2
			//test objective : has incorrect characters
			//Inputs()  = Kyle09
			//Expected outputs: invalid

			CustomerController testObject = new CustomerController(dao);
			
			try
			{
				Customer test = new Customer(0, "Kyle09", "Stewart",new Address("the moon" , "boiling rock"), "08367632","postcode16");

				testObject.addCustomer(test);
				fail("Should not get here");
				
			}
			catch(CustomerExceptionHandler c)
			{
				assertEquals("Invalid First Name, incorrect characters", c.getMessage());
			}
		}
		
		public  void testAddCustomer03()
		{
			//test firstName:3
			//test objective : string too long
			//Inputs()  = Kyleistoolongofaname
			//Expected outputs: invalid

			CustomerController testObject = new CustomerController(dao);
			
			try {
				Customer test = new Customer(0, "Kyleistoolongofaname", "Stewart",new Address("the moon" , "boiling rock"), "08367632","postcode16");

				  testObject.addCustomer(test);
				fail("Should not get here ... Exception is expected");
				
			}
			catch (CustomerExceptionHandler c) {
				
				assertEquals("Invalid First Name, must be less than 15 characters", c.getMessage());
				
			}
		}

		
		public  void testAddCustomer04()
		{
			//test lastName:1
			//test objective : incorrect characters
			//Inputs()  = Stewart77
			//Expected outputs: invalid

			CustomerController testObject = new CustomerController(dao);
			
			try {
				Customer test = new Customer(0, "Kyle", "Stewart77",new Address("the moon" , "boiling rock"), "08367632","postcode16");

				 testObject.addCustomer(test);
					fail("Should not get here ... Exception is expected");
				
				
			}
			catch (CustomerExceptionHandler c) {
				
								assertEquals("Invalid Last Name, incorrect characters", c.getMessage());
				
			}
		}

		public  void testAddCustomer05()
		{
			//test lastName:2
			//test objective : second name too long
			//Inputs()  = Stewartisareallylongname
			//Expected outputs: invalid

			CustomerController testObject = new CustomerController(dao);
			
			try {
				Customer test = new Customer(0, "Kyle", "Stewartisareallylongname",new Address("the moon" , "boiling rock"), "08367632","postcode16");

				  testObject.addCustomer(test);
				fail("Should not get here ... Exception is expected");
				
			}
			catch (CustomerExceptionHandler c) {
				
				assertEquals("Invalid last name, must be less than 15 characters", c.getMessage());
				
			}
		}
		public  void testAddCustomer06()
		{
			//test address:
			//test objective : too long address
			//Inputs()  = the moonisalongnameofcharacters" , "boiling rocksonfireoutsidethesun
			//Expected outputs: invalid

			CustomerController testObject = new CustomerController(dao); 
			
			try {
				Customer test = new Customer(0, "Kyle", "Stewart",new Address("the moonisalongnameofcharacters" , "boiling rocksonfireoutsidethesun"), "08367632","postcode16");

				  testObject.addCustomer(test);
				fail("Should not get here ... Exception is expected");
				
			}
			catch (CustomerExceptionHandler c) {
				
				assertEquals("Invalid address, must be less than 40 characters", c.getMessage());
				
			}
		}

		public  void testAddCustomer07()
		{
			//test phonenumber
			//test objective : invalid characters
			//Inputs()  = 08367632meem
			//Expected outputs: invalid

			CustomerController testObject = new CustomerController(dao);
			
			try {
				Customer test = new Customer(0, "Kyle", "Stewart",new Address("the moon" , "boiling rocks"), "08367632meem","postcode16");

				  testObject.addCustomer(test);
				
			}
			catch (CustomerExceptionHandler c) {
				
				
				assertEquals("Invalid PhoneNumber, must contain only letters", c.getMessage());
				
			}
		}

		public  void testAddCustomer08()
		{
			//test address1:2
			//test objective : phonenum too long
			//Inputs()  = 08367632420999
			//Expected outputs: too many characters

			CustomerController testObject = new CustomerController(dao);
			
			try {
				Customer test = new Customer(0, "Kyle", "Stewart",new Address("the moon" , "boiling rock"), "08367632420999","postcode16");
				  testObject.addCustomer(test);
				fail("Should not get here ... Exception is expected");
				
			}
			catch (CustomerExceptionHandler c) {
				
				assertEquals("Invalid PhoneNumber, must be less than 10 characters", c.getMessage());
				
			}
		}

		

		public  void testAddCustomer9()
		{
			//test postcode
			//test objective : invalid length
			//Inputs()  = postcode17longaddress
			//Expected outputs: valid

			CustomerController testObject = new CustomerController(dao);
			
			try {
				Customer test = new Customer(0, "Brendan", "keane",new Address("Willow24" , "House"), "087655420","postcode17longaddress");

				  testObject.addCustomer(test);
					fail("Should not get here ... Exception is expected");
				
			}
			catch (CustomerExceptionHandler c) {
				
				

				
				assertEquals("Invalid PostCode, must be less than 10 characters", c.getMessage());
				
			}
		}

		

}