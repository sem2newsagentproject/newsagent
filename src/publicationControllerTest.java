
import junit.framework.TestCase;


public class publicationControllerTest extends TestCase {
	
	private NewsAgentDAO dao;
	
		
		
		
		
		public  void testAddPublication01()
		{
			//test valid entry:
			//test objective : valid
			//Inputs()  = test object
			//Expected outputs: valid
				
			NewsAgentDAO dao = new NewsAgentDAO();
			publicationController testObject = new publicationController(dao);
			
			try {
				Publication test = new Publication(0, "Hello", "Magazine", 20.00);

				assertEquals(true,testObject.addPublication(test));
				
				
			}
			catch (PublicationExceptionHandler p) {
				
				fail("Should not get here ... Exception is expected");
				
				
			}
		}
		
		public  void testAddPublication02()
		{
			//test title:1
			//test objective : has incorrect characters
			//Inputs()  = Hello12
			//Expected outputs: invalid

			NewsAgentDAO dao = new NewsAgentDAO();
			publicationController testObject = new publicationController(dao);
			
			try
			{
				Publication test = new Publication(0, "Hello12", "Magazine", 20.00);

				testObject.addPublication(test);
				fail("Should not get here");
				
			}
			catch(PublicationExceptionHandler p)
			{
				assertEquals("Invalid, title should only contain letters", p.getMessage());
			}
		}
		
		public  void testAddPublication03()
		{
			//test title:2
			//test objective : string too long
			//Inputs()  = Helloisalongtitle
			//Expected outputs: invalid

			NewsAgentDAO dao = new NewsAgentDAO();
			publicationController testObject = new publicationController(dao);
			
			try
			{
				Publication test = new Publication(0, "Helloisalongtitle", "Magazine", 20.00);

				testObject.addPublication(test);
				fail("Should not get here");
				
			}
			catch(PublicationExceptionHandler p)
			{
				assertEquals("Invalid, too many characters in title", p.getMessage());
			}
		}

		
		public  void testAddPublication04()
		{
			//test format:1
			//test objective : incorrect characters
			//Inputs()  = Magazine34
			//Expected outputs: invalid

			NewsAgentDAO dao = new NewsAgentDAO();
			publicationController testObject = new publicationController(dao);
			
			try
			{
				Publication test = new Publication(0, "Hello", "Magazine34", 20.00);

				testObject.addPublication(test);
				fail("Should not get here");
				
			}
			catch(PublicationExceptionHandler p)
			{
				assertEquals("Invalid, format should only contain letters", p.getMessage());
			}
		}

		public  void testAddPublication05()
		{
			//test format:2
			//test objective : format too long
			//Inputs()  = Magazinehavelongnames
			//Expected outputs: invalid

			NewsAgentDAO dao = new NewsAgentDAO();
			publicationController testObject = new publicationController(dao);
			
			try
			{
				Publication test = new Publication(0, "Hello", "Magazinehavelongnames", 20.00);

				testObject.addPublication(test);
				fail("Should not get here");
				
			}
			catch(PublicationExceptionHandler p)
			{
				assertEquals("Invalid, too many characters in format", p.getMessage());
			}
		}
		
		public  void testAddPublication06()
		{
			//test price:
			//test objective : less than zero
			//Inputs()  = -56.82
			//Expected outputs: invalid

			NewsAgentDAO dao = new NewsAgentDAO();
			publicationController testObject = new publicationController(dao);
			
			try
			{
				Publication test = new Publication(0, "Hello12", "Magazine", -56.82);

				testObject.addPublication(test);
				fail("Should not get here");
				
			}
			catch(PublicationExceptionHandler p)
			{
				assertEquals("Invalid, price cannot be less than zero", p.getMessage());
			}
		}

		
		

}