public class DeliveryContent 
{
	public int pubId;
	public int deliveryId;

	public DeliveryContent(int pubId, int deliveryId)
	{
		this.pubId = pubId;
		this.deliveryId = deliveryId;
	}
	
	public int getPubId() 
	{
		return pubId;
	}
	public void setPubId(int PubId) 
	{
		this.pubId = pubId;
	}
	
	public int getDeliveryId() 
	{
		return deliveryId;
	}
	public void setDeliveryId(int DeliveryId) 
	{
		this.deliveryId = deliveryId;
	}
	
	public String toString()
	{
		return "Publication_id: " +pubId + " Delivery_id " +deliveryId ;
	}
	
	public static void main(String[] args) 
	{
	
	}

}
