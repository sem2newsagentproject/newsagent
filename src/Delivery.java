public class Delivery 
{	
	public int deliveryId;
	public int deliveryDate;
	public int cusId;
	
	public Delivery(int deliveryId, int deliveryDate, int cusId)
	{
		this.deliveryId = deliveryId;
		this.deliveryDate = deliveryDate;
		this.cusId = cusId;
	}

	public int getDeliveryId() 
	{
		return deliveryId;
	}
	public void setDeliveryId(int DeliveryId) 
	{
		this.deliveryId = deliveryId;
	}
	
	public int getDeliveryDate() 
	{
		return deliveryDate;
	}
	public void setDeliveryDate(int deliveryDate) 
	{
		this.deliveryDate = deliveryDate;
	}
	
	public int getCusId() 
	{
		return cusId;
	}
	public void setCusId(int cusId) 
	{
		this.cusId = cusId;
	}
	
	public static void main(String[] args) 
	{
	
	}

}
