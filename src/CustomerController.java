import java.sql.SQLException;
import java.util.ArrayList;
import java.util.regex.Pattern;


public class CustomerController {
	
	public CustomerController(NewsAgentDAO dao)
	{
		this.dao = dao;
	}
	
	
	private NewsAgentDAO dao;
	
	//int cusId, String firstName, String lastName, Address address, String phoneNumber, String postcode
	
	public boolean addCustomer(Customer c) throws CustomerExceptionHandler
	{
		boolean isAdded = false;
		 int fnlength = c.getFirstName().length();
		 int lnlength = c.getLastName().length();
		 
		 int numlength =c.getPhoneNumber().length();
		 
		 int alength = c.getAddress().toString().length();
		 int plength = c.getPostcode().length();
		 
			
		
		 if ( (Pattern.matches("[a-zA-Z]+", c.getFirstName()) != true)){ 
			throw new CustomerExceptionHandler("Invalid First Name, incorrect characters");}
			
		else if(fnlength > 15){
				throw new CustomerExceptionHandler("Invalid First Name, must be less than 15 characters");}
		
		else if ( (Pattern.matches("[a-zA-Z]+", c.getLastName()) != true)){ 
			throw new CustomerExceptionHandler("Invalid Last Name, incorrect characters");}
			
		else if(lnlength > 15){
				throw new CustomerExceptionHandler("Invalid last name, must be less than 15 characters");}
			
		else if(alength > 40){
			throw new CustomerExceptionHandler("Invalid address, must be less than 40 characters");}
		
		else if ( (Pattern.matches("[0-9]+", c.getPhoneNumber()) != true)){ 
			throw new CustomerExceptionHandler("Invalid PhoneNumber, must contain only letters");}
			
		else if(numlength > 10){
				throw new CustomerExceptionHandler("Invalid PhoneNumber, must be less than 10 characters");}
		
		else if(plength > 10){
			throw new CustomerExceptionHandler("Invalid PostCode, must be less than 10 characters");}
		
		
		else {
			dao.addNewCustomer(c);
		 isAdded = true;}
		 return isAdded;
	}
	
	public void removeCustomer(int i)
	{
		
		try {
			dao.removeCustomer(i);
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
	}
	
}