import java.sql.SQLException;
import java.util.ArrayList;
import java.util.regex.Pattern;

public class publicationController 
{
	//private ArrayList<Publication> pub = new ArrayList();
	
	public publicationController(NewsAgentDAO dao)
	{
		this.dao = dao;
	}

	private NewsAgentDAO dao;
	
	//add publication 
	public boolean addPublication(Publication p ) throws PublicationExceptionHandler
	{
		boolean isAdded = false;
		
		
		double price = p.getPrice();
		int title = p.getTitle().length();
		int format = p.getFormat().length();
		 
		 
		
		
		
		
		
		if(price < 0){
			throw new PublicationExceptionHandler("Invalid, price cannot be less than zero");}
		
		else if ( (Pattern.matches("[a-zA-Z]+", p.getTitle()) != true)){ 
			throw new PublicationExceptionHandler("Invalid, title should only contain letters");}
			
		else if(title > 15){
				throw new PublicationExceptionHandler("Invalid, too many characters in title");}
		
		else if ( (Pattern.matches("[a-zA-Z]+", p.getFormat()) != true)){ 
			throw new PublicationExceptionHandler("Invalid, format should only contain letters");}
			
		else if(format> 15){
				throw new PublicationExceptionHandler("Invalid, too many characters in format");}
			
		else {
			dao.addPublication(p);
			isAdded = true;}
		return isAdded;
	}
	
	public Publication getPublication(int i)
	{
		return null;
		//return pub.get(i);
	}
	
	//remove publication
	
	
	public void removePublication(int i)
	{
		try {
			dao.removePublication(i);
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		//return pub.get(i);
	}
	
	//edit publication
	public void editPublication(Publication e)
	{
		//pub.add(e);
	}
	
	public void editPublication(int i)
	{
		//return pub.get(i);
	}
	
}
