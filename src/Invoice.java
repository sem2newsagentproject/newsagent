public class Invoice 
{
	public int invId;
	public int total;
	public String date;

	public Invoice(int invId, int total, String date)
	{
		this.invId = invId;
		this.total = total;
		this.date = date;
	}
	
	public int getInvId() 
	{
		return invId;
	}
	public void setInvId(int invId) 
	{
		this.invId = invId;
	}
	
	public int getTotal() 
	{
		return total;
	}
	public void setTotal(int total) 
	{
		this.total = total;
	}
	
	public String getDate() 
	{
		return date;
	}
	public void setDate(String date) 
	{
		this.date = date;
	}
	
	public static void main(String[] args) 
	{
	
	}

}
