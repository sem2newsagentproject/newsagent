import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class NewsAgentDAO
{
	public static Connection con;
	public static java.sql.Statement stat;
	//private static CustomerController c = new CustomerController();
	//private static EmployeeController e = new EmployeeController();
	//private static publicationController p = new publicationController();
	static Statement stmt = null;
	static ResultSet rs = null;
	static Scanner in = new Scanner(System.in);
	private static String phoneNumber;
	
	Statement getStatement() {
		return stmt;
	}
	
	public NewsAgentDAO()
	{
		try{
			
			init_db();
		}
		catch(Exception e)
		{
			System.out.println("Error connecting to database..");
			e.printStackTrace();
		}
	}

	public static List<Customer> getCustomers()
	{
		List<Customer> cusList = new ArrayList<Customer>();
		try
		{
			ResultSet rs = stmt.executeQuery("SELECT * FROM customer");
			
			while (rs.next())
			{
				int cusId = rs.getInt(1);
				String firstName = rs.getString(2);
				String lastName = rs.getString(3);
				String address1 = rs.getString(4);
				String address2 = rs.getString(5);
				String phoneNumber = rs.getString(6);
				String postcode = rs.getString(7);
				Address add = new Address(address1, address2);
				Customer cus = new Customer(cusId, firstName, lastName, add, phoneNumber, postcode);
				cusList.add(cus);
			}
		}
		catch(Exception e)
		{
			System.out.println("Error Getting");
			e.printStackTrace();
		}
		return cusList;
	}
	
	
	public static List<Employee> getEmployees()
	{
		List<Employee> empList = new ArrayList<Employee>();
		try
		{
			ResultSet rs = stmt.executeQuery("SELECT * FROM employee");
			
			while (rs.next())
			{
				int EmpID = rs.getInt(1);
				String firstname = rs.getString(2);
				String lastname = rs.getString(3);
				String address1 = rs.getString(4);
				String address2 = rs.getString(5);
				String phonenum = rs.getString(6);
				Address add = new Address(address1, address2);
				Employee emp = new Employee(EmpID ,firstname, lastname, add, phonenum);
				empList.add(emp);
			}
		}
		catch(Exception e)
		{
			System.out.println("Error Getting");
			e.printStackTrace();
		}
		return empList;
	}
	
	public static List<Publication> getPublication()
	{
		List<Publication> pubList = new ArrayList<Publication>();
				try
				{
					ResultSet rs = stmt.executeQuery("SELECT * FROM publication");
					
					while(rs.next())
					{
						int pubId = rs.getInt(1);
						double price = rs.getDouble(3);
						String title = rs.getString(2);
						String format = rs.getString(4);						
						Publication pub = new Publication(pubId, title, format, price);
						pubList.add(pub);
					}
				}
				catch(Exception e)
				{
					System.out.println("Error Getting");
					e.printStackTrace();
				}
				return pubList;
	}
	
	
	public static void addNewCustomer(Customer cust)
	{
		
		try {
		//c.addCustomer(cust);
		//int i  =  c.getSize() - 1;
		//c.getCustomer(i).getCusId();
		
		//int cusId = c.getCustomer(0).getCusId();
		String firstName = cust.getFirstName();
		String lastName = cust.getLastName();
		Address address = cust.getAddress();
		//String address2 = c.getCustomer(i).getAddress2();
		String phoneNumber = cust.getPhoneNumber();
		String postCode = cust.getPostcode();
		
        String str="INSERT INTO customer VALUES ('"+0+"', '"+firstName+"', '"+lastName+"', '"+address.getAddress1()+"', '"+address.getAddress2()+"', '"+phoneNumber+"', '"+postCode+"');";
        
			stmt.executeUpdate(str);
		} catch (SQLException e) {
			System.out.println("Error adding customer");
			e.printStackTrace();
		}
	}
	
	
	public void addNewEmployee(Employee emp)
	{
		Statement localstmt = getStatement();
		
		try {
		/*e.addEmployee(emp);
		int i  =  e.getSize() - 1;
		e.viewEmployee(i).getEmpID();*/
		
		//int cusId = c.getCustomer(0).getCusId();
		String firstname = emp.getFirstName();
		String lastname = emp.getLastName();
		Address address = emp.getAddress();
		//String address2 = c.getCustomer(i).getAddress2();
		String phonenum = emp.getPhoneNumber();
		
        String str="INSERT INTO employee VALUES ('"+0+"', '"+firstname+"', '"+lastname+"', '"+address.getAddress1()+"', '"+address.getAddress2()+"', '"+phonenum+"');";
        
			localstmt.executeUpdate(str);
			return;
		} catch (SQLException e) {
			System.out.println("Error adding employee");
			e.printStackTrace();
			return;
		}
	}
	
	public static void removeCustomer(int i) throws SQLException
	{
		int toRemove = i;
		String str = "DELETE FROM customer WHERE cus_id = "+toRemove+";";
		
		stmt.executeUpdate(str);
	}
	
	public static void removeEmployee(int i) throws SQLException
	{
		int toRemove = i;
		String str = "DELETE FROM employee WHERE emp_id = "+toRemove+";";
		
		stmt.executeUpdate(str);
	}
	
	public static void removePublication(int i) throws SQLException
	{
		int toRemove = i;
		String str = "DELETE FROM publication WHERE pub_id = "+toRemove+";";
		
		stmt.executeUpdate(str);
	}
	
	public void addPublication(Publication pub) 
	{
		 try {
		//p.addPublication(pub);
		//int i  =  p.getSize() - 1;
		//p.getPublication(i).getPubId();
		
		//int pubId = p.getPublication(0).getPubId();
		double price = pub.getPrice();
		String title = pub.getTitle();
		String format = pub.getFormat();
		
		String str = "INSERT INTO publication VALUES ('"+0+"', '"+title+"',  '"+price+"','"+format+"');";
       
			stmt.executeUpdate(str);
		} catch (SQLException e) {
			System.out.println("Error adding publication");
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) throws SQLException 
	{
		
		  // open the connection to the database
		/*addNewCustomer(0);
		List<Customer> Clist = getCustomers();
		for(Customer c: Clist)
		{
			System.out.println(c.toString());
		}
		System.out.println();
		
		addNewEmployee(0);
		List<Employee> Elist = getEmployees();
		for(Employee e: Elist)
		{
			System.out.println(e.toString());
		}
		System.out.println();
		
		//addPublications(0);
		List<Publication> list = getPublication();
		for(Publication p: list)
		{
		  System.out.println(p.toString());
		}
		System.out.println();*/
	}
	public static void init_db()
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			String url="jdbc:mysql://localhost:3305/newsagents";
			con = DriverManager.getConnection(url, "root", "admin");
			stmt = con.createStatement();
		}
		catch(Exception e)
		{
			System.out.println("Error: Failed to connect to database\n"+e.getMessage());
		}
	}
}