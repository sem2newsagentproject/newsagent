import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

public class UserInterface 
{	
	NewsAgentDAO database;
	CustomerController c;
	EmployeeController e;
	publicationController p;
	
	public UserInterface(NewsAgentDAO dao) 
	{
		this.database = dao;
		c=new CustomerController(dao);
		e=new EmployeeController(dao);
		p=new publicationController(dao);
	}
	
	public void start() throws SQLException, EmployeeExceptionHandler, CustomerExceptionHandler, PublicationExceptionHandler
	{
		Scanner in = new Scanner(System.in);
		
		System.out.println("\nEnter 1 to view all customers");
		System.out.println("\nEnter 2 to view all employees");
		System.out.println("\nEnter 3 to view all publications");
		System.out.println("\nEnter 4 to add a customer");
		System.out.println("\nEnter 5 to add an employee");
		System.out.println("\nEnter 6 to add a publication");
		System.out.println("\nEnter 7 to remove a customer");
		System.out.println("\nEnter 8 to remove an employee");
		System.out.println("\nEnter 9 to remove an publication");
		
		System.out.println("\nEnter a choice (-1 to quit)");
		int choice = in.nextInt();
		
		while(choice != -1)
		{
			if(choice == 1)
			{
				List<Customer> cusList = database.getCustomers();
				for (Customer cus: cusList)
					System.out.println(cus.toString());
			}
			else if (choice == 2)
			{
				List<Employee> empList = database.getEmployees();
				for (Employee emp: empList)
					System.out.println(emp.toString());
			}
			else if (choice == 3)
			{
				List<Publication> pubList = database.getPublication();
				for (Publication pub: pubList)
					System.out.println(pub.toString());
			}
			
			else if (choice == 4)
			{
				try{
					
					System.out.println("Enter customer first name: ");
					String firstName = in.next();
					System.out.println("Enter customer last name: ");
					String lastName = in.next();
					System.out.println("Enter customer address1: ");
					String address1 = in.next();
					System.out.println("Enter customer address2: ");
					String address2 = in.next();
					Address address=new Address(address1, address2);
					System.out.println("Enter customer phone number: ");
					String phoneNumber = in.next();
					System.out.println("Enter customer postcode: ");
					String postCode = in.next();
					Customer cus = new Customer(0, firstName, lastName, address, phoneNumber, postCode);
					//database.addNewCustomer(cus);
					c.addCustomer(cus);
				}
				catch (CustomerExceptionHandler c){
					System.out.println(c.message);
				}
			}
			
			else if (choice == 5)
			{
				try{
					
					
					System.out.println("Enter employee first name: ");
					String firstName = in.next();
					System.out.println("Enter employee last name: ");
					String lastName = in.next();
					System.out.println("Enter employee address1: ");
					String address1 = in.next();
					System.out.println("Enter employee address2: ");
					String address2 = in.next();
					Address address=new Address(address1, address2);
					System.out.println("Enter employee phone number: ");
					String phoneNumber = in.next();
					Employee emp = new Employee(0, firstName, lastName, address, phoneNumber);
					//database.addNewEmployee(emp);
					e.addEmployee(emp);
				}
				catch (EmployeeExceptionHandler e){
					System.out.println(e.message);
				}
			}
			
			else if (choice == 6)
			{
				try{
				System.out.println("Enter publication title: ");
				String title = in.next();
				System.out.println("Enter publication format: ");
				String format = in.next();
				System.out.println("Enter publication price: ");
				double price = in.nextDouble();
				Publication pub = new Publication(0, title, format, price);
				//database.addPublication(pub);
				p.addPublication(pub);
				}
				catch(PublicationExceptionHandler p) {
					System.out.println(p.message);
				}
			}
			
			else if (choice == 7)
			{
				System.out.println("Enter customer id: ");
				int C_id = in.nextInt();
				
				c.removeCustomer(C_id);
			}
			
			
			else if (choice == 8)
			{
				System.out.println("Enter employee id: ");
				int E_id = in.nextInt();
				
				e.removeEmployee(E_id);
			}
			
			else if (choice == 9)
			{
				System.out.println("Enter publication id: ");
				int P_id = in.nextInt();
				
				p.removePublication(P_id);
			}
			else
			{
				System.out.println("You entered an incorrect number");
			}
			
			
			System.out.println("\nEnter 1 to view all customers");
			System.out.println("\nEnter 2 to view all employees");
			System.out.println("\nEnter 3 to view all publications");
			System.out.println("\nEnter 4 to add a customer");
			System.out.println("\nEnter 5 to add an employee");
			System.out.println("\nEnter 6 to add a publication");
			System.out.println("\nEnter 7 to remove a customer");
			System.out.println("\nEnter 8 to remove an employee");
			System.out.println("\nEnter 9 to remove an publication");
			System.out.println("\nEnter a choice (-1 to quit)");
			choice = in.nextInt();
		}
	}
}