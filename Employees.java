public class Employees 
{
	public int empId;
	public String firstName;
	public String lastName;
	public String address1;
	public String address2;
	public String phoneNumber;
	
	public Employees(int empId, String firstName, String lastName, String address1, String address2, String phoneNumber)
	{
		this.empId = empId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.address1 = address1;
		this.address2 = address2;
		this.phoneNumber = phoneNumber;
	}
	
	public int getEmpId() 
	{
		return empId;
	}
	public void setEmpId(int empId) 
	{
		this.empId = empId;
	}

	public String getFirstName() 
	{
		return firstName;
	}
	public void setFirstName(String firstName) 
	{
		this.firstName = firstName;
	}

	public String getLastName() 
	{
		return lastName;
	}
	public void setLastName(String lastName) 
	{
		this.lastName = lastName;
	}

	public String getAddress1() 
	{
		return address1;
	}
	public void setAddress1(String address1) 
	{
		this.address1 = address1;
	}

	public String getAddress2() 
	{
		return address2;
	}
	public void setAddress2(String address2) 
	{
		this.address2 = address2;
	}

	public String getPhoneNumber() 
	{
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) 
	{
		this.phoneNumber = phoneNumber;
	}
	
	public static void main(String[] args) 
	{
	
	}

}
