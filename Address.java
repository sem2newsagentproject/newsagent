
public class Address implements CharSequence {
	
	private String address1;
	private String address2;
	
	public Address(String a1, String a2)
	{
		address1 = a1;
		address2 = a2;
	}
	
	public String toString()
	{
		return address1 +", "+ address2;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public char charAt(int index) {
		// TODO Auto-generated method stub
		return 0;
	}

	public int length() {
		// TODO Auto-generated method stub
		return 0;
	}

	public CharSequence subSequence(int start, int end) {
		// TODO Auto-generated method stub
		return null;
	}

}
